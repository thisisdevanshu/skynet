from keras.models import load_model
from keras.preprocessing import image
import keras
import numpy as np
import argparse

classifier = load_model('model')

test_image = image.load_img('LD_Test/phy.jpg', target_size = (300, 300))
parser = argparse.ArgumentParser()
parser.add_argument(
	'--image', type=str, default='', help='Image Path')
args = parser.parse_args()

if args.image:
	test_image = image.load_img(args.image, target_size = (300, 300))

test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = classifier.predict(test_image)
result_class = result.argmax(axis = 1)
if(result_class == 0):
	print('Architecture')
if(result_class == 1):
	print('Ben Hill Griffin Staidum')
if(result_class == 2):
	print('Benton Hall')
if(result_class == 3):
	print('Black Hall')
if(result_class == 4):
	print('Century Tower')
if(result_class == 5):
	print('CSE')
if(result_class == 6):
	print('Hub')
if(result_class == 7):
	print('Library West')
if(result_class == 8):
	print('Little Hall')
if(result_class == 9):
	print('Marston Library')
if(result_class == 10):
	print('NEB')
if(result_class == 11):
	print('Physics')
if(result_class == 12):
	print('Reitz Union')
if(result_class == 13):
	print('Turlington Hall')

print(result[0])
