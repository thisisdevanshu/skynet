package com.example.arnavaima.landmarkdetection;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by arnavaima on 4/26/18.
 */

public interface RetrofitInterface {

    String BASE_URL = "http://192.168.0.24:8080/";
    //String BASE_URL = Constants.urlBaseServer;
    //String BASE_URL = "http://172.31.43.110:8080/";

    @Multipart
    @POST
    Call<MyResponse> postImage(@Header("authorization") String auth, @Part MultipartBody.Part image, @Part("image") RequestBody name, @Url String url);

/*
    @Multipart
    @POST("uploadAttachment")
    Call<MyResponse> uploadImage(@Part MultipartBody.Part filePart);
    */

    /*
    @Multipart
    @POST("Api.php?apicall=upload")
    Call<MyResponse> uploadImage(@Part("image\"; filename=\"myfile.jpg\" ") RequestBody file, @Part("desc") RequestBody desc);*/
}
