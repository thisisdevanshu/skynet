package com.example.arnavaima.landmarkdetection;

/**
 * Created by arnavaima on 4/21/18.
 */

public class Constants {

    public static final String urlLogin = "http://52.24.43.197:8080/login";
    public static final String urlImageServer = "http://52.24.43.197:8080/landmark";
    public static final String urlBaseServer = "http://52.24.43.197:8080/";
    public static final String keyAuth = "authorization";

}
