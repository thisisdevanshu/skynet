package com.example.arnavaima.landmarkdetection;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private LoginAuthenticationTask mAuthTask = null;
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginView;

    private String mAuthToken;

    private static final int REQUEST_STORAGE_PERMISSION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.landmark_detection);
        setSupportActionBar(toolbar);

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }


        mLoginView = findViewById(R.id.login_view);
        mProgressView = findViewById(R.id.login_progress);

        mUsernameView = findViewById(R.id.username);
        mPasswordView = findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        Button loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnectedToWifi() || isConnectedToNetwork()) {
                    attemptLogin();
                } else {
                    Toast.makeText(MainActivity.this, "Network connection not available", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //start audio recording or whatever you planned to do
            }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.RECORD_AUDIO)) {
                    //Show an explanation to the user *asynchronously*
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("This permission is important to access you Gallery.")
                            .setTitle("Important permission required");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_STORAGE_PERMISSION);
                        }
                    });
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_STORAGE_PERMISSION);
                }else{
                    //Never ask again and handle your app without permission.
                }
            }
        }
    }

    private void attemptLogin() {

       if (mAuthTask != null) {
           return;
       }

        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean invalidLogin = false;
        View viewInFocus = null;

        if(TextUtils.isEmpty(username)) {
            viewInFocus = mUsernameView;
            invalidLogin = true;

        }

        if(TextUtils.isEmpty(password)) {
            viewInFocus = mPasswordView;
            invalidLogin = true;

        }

        if(invalidLogin) {
            viewInFocus.requestFocus();
        } else {
            showProgress(true);
            mAuthTask = new LoginAuthenticationTask(username, password);
            mAuthTask.execute((Void) null);
        }

    }

    private void showProgress(final boolean show) {

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginView.setVisibility(show ? View.GONE : View.VISIBLE);

    }

    private boolean isConnectedToWifi() {

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        return false;

    }

    private boolean isConnectedToNetwork() {

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        return false;

    }

    public class LoginAuthenticationTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        LoginAuthenticationTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            HttpURLConnection conn = null;
            StringBuilder sbuf = new StringBuilder();
            sbuf.append("{\"username\":\"");
            sbuf.append(mUsername);
            sbuf.append("\",\"password\":\"");
            sbuf.append(mPassword);
            sbuf.append("\"}");

            System.out.println("aaaaaString:"+sbuf.toString());

            try {

                URL url = new URL(Constants.urlLogin);

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type","application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(sbuf.toString());
                writer.flush();
                conn.connect();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                System.out.println("Landmark Detection : Login : respnse code: "+responseCode);

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                   mAuthToken = conn.getHeaderField(Constants.keyAuth);
                   return true;

               } else {
                   return false;
                }

            } catch (Exception e) {
                System.out.println("arnav incatch");
                e.printStackTrace();
                return false;
            } finally {

                if (conn != null) {
                    conn.disconnect();
                }
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                Intent intent = new Intent(MainActivity.this, ImageTestActivity.class);
                intent.putExtra(Constants.keyAuth, mAuthToken);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(MainActivity.this, "User authentication failed.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

}
