package com.example.arnavaima.landmarkdetection;

import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;

/**
 * Created by arnavaima on 4/26/18.
 */

public class MyResponse {

    String message;
    String landmark;

    public String getMessage() {
        return this.message;

    }
    public void setMessage(String message) {
        this.message = message;

    }
    public String getLandmark() {
        return this.landmark;

    }
    public void setLandmark(String landmark) {
        this.landmark = landmark;

    }

}
