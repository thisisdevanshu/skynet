package com.example.arnavaima.landmarkdetection;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageTestActivity extends AppCompatActivity {

    private static final int REQUEST_GALLERY_UPLOAD = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;

    private boolean isGalleryUpload = false;
    private boolean validResult = false;

    private String mCurrentPhotoPath;
    private String mGalleryPhotoPath;
    private String mAuthToken;
    private Uri cameraImgUri = null;
    private Uri galleryImgUri = null;

    private Button mCheckButton;
    private ImageView mImageView;
    TextView resultView;

    private View mUploadView;
    private View mProgressView;

    RetrofitInterface ret;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_test);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.landmark_detection);
        setSupportActionBar(toolbar);

        mAuthToken = getIntent().getStringExtra(Constants.keyAuth);

        mUploadView = findViewById(R.id.img_upload_view);
        mProgressView = findViewById(R.id.img_progress);
        resultView = findViewById(R.id.textView);

        mCheckButton = findViewById(R.id.checkButton);
        mImageView = findViewById(R.id.imageView);

        Button galleryButton = findViewById(R.id.galleryButton);
        Button cameraButton = findViewById(R.id.cameraButton);

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGalleryIntent();
            }
        });

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCameraIntent();
            }
        });

        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadFile(cameraImgUri, "image", Constants.urlImageServer);
            }
        });
        mCheckButton.setVisibility(View.INVISIBLE);

    }

    private void startGalleryIntent() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_GALLERY_UPLOAD);

    }

    private void startCameraIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }


    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String imagePath = cursor.getString(column_index);

        return imagePath;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //String imageFileName = new String("myfile");
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        cameraImgUri = Uri.fromFile(f);
        mediaScanIntent.setData(cameraImgUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void showProgress(final boolean show) {

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mUploadView.setVisibility(show ? View.GONE : View.VISIBLE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        validResult = false;

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            galleryAddPic();
            mImageView.setImageURI(cameraImgUri);

            validResult = true;
            isGalleryUpload = false;
        }

        if(requestCode == REQUEST_GALLERY_UPLOAD && resultCode == RESULT_OK) {

            galleryImgUri = data.getData();
            mImageView.setImageURI(galleryImgUri);
            mGalleryPhotoPath = getPath(galleryImgUri);
            validResult = true;
            isGalleryUpload = true;

        }

        if(validResult) {
            mCheckButton.setVisibility(View.VISIBLE);
        } else {
            mCheckButton.setVisibility(View.INVISIBLE);
        }

    }

    private void uploadFile(Uri fileUri, String desc, String url) {

        showProgress(true);

        OkHttpClient client = new OkHttpClient.Builder().build();

        // Change base URL to your upload server URL.
         ret = new Retrofit.Builder().baseUrl(RetrofitInterface.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build().create(RetrofitInterface.class);

         String photoPath;

         File file = null;

         if(isGalleryUpload && validResult) {
             file = new File(mGalleryPhotoPath);
             //photoPath = mGalleryPhotoPath;
         } else {
             file = new File(mCurrentPhotoPath);
             //photoPath = mCurrentPhotoPath;
         }

         //File file = new File(photoPath);

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");


        retrofit2.Call<MyResponse> req = ret.postImage(mAuthToken, body, name, Constants.urlImageServer);
        req.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                showProgress(false);
                if(response.isSuccessful()) {
                    String res = response.body().getLandmark();
                    resultView.setText("You are at "+res+".");
                    System.out.println(res);
                } else {System.out.println("-------done");}
                // Do Something
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {

                showProgress(false);
                t.printStackTrace();
            }
        });

    }

}
