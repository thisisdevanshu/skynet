package com.dev.SkyNet;


public class Response {
     private String message;
     private String landmark;

    public String getLandmark() {
        return landmark;
    }

    public String getMessage() {
        return message;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
