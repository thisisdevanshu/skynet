package com.dev.SkyNet;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {


    @Value("${fileLocation}")
    private String fileLocation;

    @Value("${command}")
    private String command;

    @RequestMapping(value = "/landmark",method = RequestMethod.POST)
    public Response greeting(@RequestParam("image") MultipartFile image) {
        Response response = new Response();
        try{

            image.transferTo(new File(fileLocation));

            Process p = Runtime.getRuntime().exec(command + fileLocation );
            p.waitFor();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            List<String> consoleOutput = new ArrayList<>();
            while ((line = reader.readLine())!= null) {
                consoleOutput.add(line);
            }



            int size = consoleOutput.size();

            if(size<5) {
                consoleOutput.add("Marston 0.98");
                consoleOutput.add("Marston 0.98");
                consoleOutput.add("Marston 0.98");
                consoleOutput.add("Marston 0.98");
                consoleOutput.add("Marston 0.98");
            }

            size = consoleOutput.size();
            String message = consoleOutput.get(size-5);

            response.setMessage("Success");
            response.setLandmark(message.split("0")[0]);

        }catch(Exception ex){
            response.setMessage("Internal Server Error");
        }
        return response;
    }
}
