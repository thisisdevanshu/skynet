from keras.models import Sequential

from keras.layers import Conv2D

from keras.layers import MaxPooling2D

from keras.layers import Flatten

from keras.layers import Dense

from keras.models import Model

from keras.preprocessing.image import ImageDataGenerator

from keras.layers import Input
import keras

input_img = Input(shape = (300, 300, 3))

tower_1 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img)
tower_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1)

tower_2 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img)
tower_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_2)

tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input_img)
tower_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_3)
inception1 = keras.layers.concatenate([tower_1, tower_2, tower_3], axis = 1)


tower_1 = Conv2D(64, (1,1), padding='same', activation='relu')(inception1)
tower_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1)

tower_2 = Conv2D(64, (1,1), padding='same', activation='relu')(inception1)
tower_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_2)

tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(inception1)
tower_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_3)
inception2 = keras.layers.concatenate([tower_1, tower_2, tower_3], axis = 1)

tower_1 = Conv2D(64, (1,1), padding='same', activation='relu')(inception2)
tower_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1)

tower_2 = Conv2D(64, (1,1), padding='same', activation='relu')(inception2)
tower_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_2)

tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(inception2)
tower_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_3)
inception3 = keras.layers.concatenate([tower_1, tower_2, tower_3], axis = 1)
inception3 = Flatten()(inception3)
inception3 = Dense(14, activation='softmax')(inception3)

classifier = Model(inputs = input_img, outputs = inception3)

classifier.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])

train_datagen = ImageDataGenerator(rescale = 1./255,
shear_range = 0.2,
zoom_range = 0.2,
horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)
training_set = train_datagen.flow_from_directory('LD',
target_size = (300, 300),
batch_size = 32,
class_mode = 'categorical')
test_set = test_datagen.flow_from_directory('LD_Test',
target_size = (300, 300),
batch_size = 32,
class_mode = 'categorical')


classifier.fit_generator(training_set,
steps_per_epoch = 4000,
epochs = 1,
validation_data = test_set,
validation_steps = 200)

classifier.save('model')
